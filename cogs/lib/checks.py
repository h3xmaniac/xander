from discord.ext import commands

# Copyright (C) Flumboni - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
def check_flum():
    async def predicate(ctx):
        return ctx.author.id != 344273597612818444
    return commands.check(predicate)

def check_bot():
    async def predicate(ctx):
        return not ctx.author.bot
    return commands.check(predicate)

def _check():
    async def predicate(ctx):
        ids = [232948417087668235,124316478978785283]
        return ctx.author.id in ids
    return commands.check(predicate)